﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class guardia1 : MonoBehaviour
{
    bool mover= false;
    int rapidez = 5;

    void Update()
    {
        if (transform.position.x >= -48)
        {
            mover = true;
        }
        if (transform.position.x <= -53)
        {
            mover = false;
        }

        if (mover)
        {
            der();
        }
        else
        {
            iz();
        }

    }

    void iz()
    {
        transform.position += transform.right * rapidez * Time.deltaTime;
    }

    void der()
    {
        transform.position -= transform.right * rapidez * Time.deltaTime;
    }

}
