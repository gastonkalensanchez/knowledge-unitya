﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cubo : MonoBehaviour
{
    public Explocion ex;

    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("cubo"))
        {
            ex.enabled = true;
        }
    }
}
