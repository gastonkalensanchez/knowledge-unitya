﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    public GameObject bala;
    public Camera Camara;
       
    public GameObject pro;
    
    void Start()
    {
        
        
        StartCoroutine("Esperar");
    }


 
    IEnumerator Esperar()
    {
        bool a = true;
        while (a)
        {
            yield return new WaitForSeconds(1.5f);
            Ray rayo = Camara.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            
            pro = Instantiate(bala, rayo.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(Camara.transform.forward * 50, ForceMode.Impulse);

            Destroy(pro, 3);


        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(pro);
        }
    }
}
