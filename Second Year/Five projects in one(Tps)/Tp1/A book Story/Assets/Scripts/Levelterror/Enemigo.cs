﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    private int hp;
    public Explocion ex;
    void Start()
    {
        hp = 10;
    }
    private void damage()
    {
        hp = hp - 10;
        if (hp <= 0)
        {
            ex.enabled = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("arma"))
        {
            damage();
        }
    }
}
