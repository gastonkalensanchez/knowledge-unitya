﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPlayer : MonoBehaviour
{
    public float vertical;
    public float horizontal;
    public CharacterController player;
    public float playerSpeed;

    private void Start()
    {
        player = GetComponent<CharacterController>();
    }

    private void Update()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
    }

    private void FixedUpdate()
    {
      
        player.Move(transform.TransformDirection(new Vector3(horizontal, 0, vertical)) * playerSpeed * Time.deltaTime);
    }

}
