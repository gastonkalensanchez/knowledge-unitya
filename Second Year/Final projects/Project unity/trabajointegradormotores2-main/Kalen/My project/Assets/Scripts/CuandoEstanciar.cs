﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuandoEstanciar : MonoBehaviour
{
    public GameObject Spawn;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            Spawn.SetActive(true);
        }
    }
}
