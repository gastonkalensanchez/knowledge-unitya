﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jefe : MonoBehaviour
{
    public Camera cam, cam2, cam3;
    public GameObject proyectil;
    void Start()
    {
        StartCoroutine("Esperar");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator Esperar()
    {
        bool a = true;

        while (a) 
        {
            yield return new WaitForSeconds(3f);


            Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(proyectil, ray.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(cam.transform.forward * 5, ForceMode.Impulse);

            Destroy(pro, 5);



            Ray ray2 = cam2.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro2;
            pro2 = Instantiate(proyectil, ray2.origin, transform.rotation);

            Rigidbody rb2 = pro2.GetComponent<Rigidbody>();
            rb2.AddForce(cam2.transform.forward * 5, ForceMode.Impulse);

            Destroy(pro2, 5);



            Ray ray3 = cam3.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro3;
            pro3 = Instantiate(proyectil, ray3.origin, transform.rotation);

            Rigidbody rb3 = pro3.GetComponent<Rigidbody>();
            rb3.AddForce(cam3.transform.forward * 5, ForceMode.Impulse);

            Destroy(pro3, 5);
        }

        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador") || other.gameObject.CompareTag("Jugador2"))
        {

            Destroy(gameObject);

        }
    }
}
