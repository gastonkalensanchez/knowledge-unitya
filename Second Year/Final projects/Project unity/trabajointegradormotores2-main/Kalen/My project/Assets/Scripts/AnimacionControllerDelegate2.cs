﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacionControllerDelegate2 : MonoBehaviour
{

    public GameObject ProyectilPj;
    public Transform PuntoDeSalidaPj;

    public GameObject Mano_Derecha_Punto, Mano_Izquierda_Punto, Pie_Derecho_Punto,Bate_Punto,BoomPunto;

    internal bool EnemigoNoqueado;
  
  

    private AnimacionController AnimationScript;

    private EnemigoMov MovimientoDelEnemigo;
    private PlayerMov2 MovimientoDelJugador;
    private PlayerAtack2 AtaqueDelJugador;
    public GameObject Jugador;
   

    void Awake()
    {
      
        AnimationScript = GetComponent<AnimacionController>();
        if (gameObject.CompareTag(Tags.Enemigo_Tag))
        {
            MovimientoDelEnemigo = GetComponentInParent<EnemigoMov>();
            
        }
        if (gameObject.CompareTag("Jugador2"))
        {
            MovimientoDelJugador = GetComponentInParent<PlayerMov2>();
            AtaqueDelJugador = GetComponentInParent<PlayerAtack2>();
        }
       
        
    }
 
    void ProyectilPersonaje()
    {
        GameObject pro;
        pro = Instantiate(ProyectilPj, PuntoDeSalidaPj.transform.position, transform.rotation);

        Rigidbody rb = pro.GetComponent<Rigidbody>();
        rb.AddForce(PuntoDeSalidaPj.transform.forward * 15, ForceMode.Impulse);

        Destroy(pro, 5);


    }
    void Bate_PuntoON()
    {
        Bate_Punto.SetActive(true);
    }
    void Bate_PuntoOFF()
    {
        Bate_Punto.SetActive(false);
    }
    void Boom_PuntoON()
    {
        BoomPunto.SetActive(true);
    }
    void Boom_PuntoOFF()
    {
        BoomPunto.SetActive(false);
    }
    void Mano_Derecha_PuntoON()
    {
        Mano_Derecha_Punto.SetActive(true);
    }
    void Mano_Derecha_PuntoOFF()
    {
        if (Mano_Derecha_Punto.activeInHierarchy)
        {
            Mano_Derecha_Punto.SetActive(false);
        }
        
    }

    void Mano_Izquierda_PuntoON()
    {
        Mano_Izquierda_Punto.SetActive(true);
    }
    void Mano_Izquierda_PuntoOFF()
    {
        if (Mano_Izquierda_Punto.activeInHierarchy)
        {
            Mano_Izquierda_Punto.SetActive(false);
        }
    }

    void Pie_Derecho_PuntoON()
    {
        Pie_Derecho_Punto.SetActive(true);
    }

    void Pie_Derecho_PuntoOFF()
    {
        if (Pie_Derecho_Punto.activeInHierarchy)
        {
            Pie_Derecho_Punto.SetActive(false);
        }
    }

    void TagManoDerecha()
    {

        Mano_Derecha_Punto.tag = Tags.ManoR_Tag;
    }
    void unTagManoDerecha()
    {
        Mano_Derecha_Punto.tag = Tags.Untagged_Tag;
    }
    void TagPiernaDerecha()
    {

        Pie_Derecho_Punto.tag = Tags.PiernaR_Tag;
    }
    void unTagPiernaDerecha()
    {
        Pie_Derecho_Punto.tag = Tags.Untagged_Tag;
    }

    void EnemigoLevantando()
    {
        StartCoroutine(LevantarseDespuesDeUntiempo());
    }


    IEnumerator LevantarseDespuesDeUntiempo()
    {
        yield return new WaitForSeconds(0.5f);
        AnimationScript.Levantarse();
    }
    void PrenderMovimientoEnemigo()
    {
        MovimientoDelEnemigo.enabled = true;
        EnemigoNoqueado = false;
        
        //MovimientoPlayer.enabled = true;
        //pone el layer enemigo
        transform.parent.gameObject.layer = 9;
    }
    void CancelarMovimientoEnemigo()
    {
        EnemigoNoqueado = true;
        MovimientoDelEnemigo.enabled = false;
        
        //MovimientoPlayer.enabled = false;
        //pone el leyer en default
        transform.parent.gameObject.layer = 0;
    }
    void PrenderMovimientoJugador()
    {
        MovimientoDelJugador.enabled = true;
        
    }
    void CancelarMovimientoJugador()
    {
        MovimientoDelJugador.enabled = false;
       
    }
    void PrenderMovimientoJugadorH()
    {
        MovimientoDelJugador.enabled = true;
        AtaqueDelJugador.enabled = true;
        Jugador.tag = "Jugador2";
        gameObject.tag = "Jugador2";
    }
    void CancelarMovimientoJugadorH()
    {
        MovimientoDelJugador.enabled = false;
        AtaqueDelJugador.enabled = false;       
        Jugador.tag = "Nada";
        gameObject.tag = "Nada";
    }

    void Muerte()
    {
        Invoke("DesactivarGameObjet", 2f);
    }
    void DesactivarGameObjet()
    {
        gameObject.SetActive(false);
    }
  
}
