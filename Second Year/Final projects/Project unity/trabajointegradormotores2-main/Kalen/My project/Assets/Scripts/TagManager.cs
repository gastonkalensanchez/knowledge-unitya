﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimTags
{
    public const string Movimiento = "Caminar";

    public const string Golpe1_Trigger = "Golpe1";
    public const string Golpe2_Trigger = "Golpe2";
    public const string Golpe3_Trigger = "Golpe3";

    public const string Patada1_Trigger = "Patada1";
    public const string Patada2_Trigger = "Patada2";
    public const string PatadaEnElAire_Trigger = "PatadaEnElAire";
    public const string Combo1_Trigger = "Combo1";
    public const string Combo2_Trigger = "Combo2";
    public const string Combo3_Trigger = "Combo3";

    public const string IDLE = "idle";

    public const string Noqueado_Trigger = "Noqueado";
    public const string Levantarse_Trigger = "Levantarse";
    public const string Golpeado_Trigger = "Golpeado";
    public const string Death_Trigger = "Muerto";
    public const string Salto_Tag = "Salto";
    public const string Ensalto_Tag = "Ensalto";

}



    public class Axis
    {
        public const string Horizontal_Axis = "Horizontal";
        public const string Vertical_Axis = "Vertical";
        public const string JHorizontal_Axis = "JHorizontal";
        public const string JVertical_Axis = "JVertical";
}

public class Tags
{
    public const string Piso_Tag = "Piso";
    public const string Jugador_Tag = "Jugador";
    public const string Enemigo_Tag = "Enemigo";

    public const string ManoR_Tag = "ManoR";
    public const string PiernaR_Tag = "PiernaR";
    public const string Untagged_Tag = "Untagged";
   
    public const string Vida_UI = "VidaUI";

    




}

