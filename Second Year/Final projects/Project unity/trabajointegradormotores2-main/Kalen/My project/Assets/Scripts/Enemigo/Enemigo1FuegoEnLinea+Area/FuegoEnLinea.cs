﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuegoEnLinea : MonoBehaviour
{
    public GameObject PaticulasFuego,Anticipacion;
    public GameObject SpawnParticulas;
    private EnemigoMov Movimiento;

    private AnimacionControllerDelegate a;

    public float contador = 0;
    void Start()
    {
        Movimiento = GetComponentInParent<EnemigoMov>();
        a = GetComponent<AnimacionControllerDelegate>();
    }

   
    void Update()
    {
        if (Movimiento.PlayerTarget.CompareTag("Jugador") || Movimiento.PlayerTarget.CompareTag("Jugador2"))
        {
            contador += 1 * Time.deltaTime;
            if (contador >= 3 && a.EnemigoNoqueado == false)
            {
                Mostrar();
            }
            if (contador >= 6f && a.EnemigoNoqueado == false)
            {
                MostrarFuego();
                contador = 0;
            }
            if (Vector3.Distance(transform.position, Movimiento.PlayerTarget.position) <= 1f)
            {

                Movimiento.rb.velocity = Vector3.zero; //DetieneElmovimiento
                Movimiento.EnemigoAnimacion.Correr(false);
                Movimiento.SeguirJugador = false;
                Movimiento.AtacarJugador = true;
            }
        }
        
        
    }
    public void MostrarFuego()
    {
        GameObject particulas = Instantiate(PaticulasFuego, SpawnParticulas.transform.position, SpawnParticulas.transform.rotation) as GameObject;
        Destroy(particulas, 1f);

    }
    public void Mostrar()
    {
        GameObject particulas2 = Instantiate(Anticipacion, SpawnParticulas.transform.position, SpawnParticulas.transform.rotation) as GameObject;
        Destroy(particulas2, 0.5f);

    }
}
