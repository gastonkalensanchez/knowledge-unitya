﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacionController : MonoBehaviour
{
    private Animator animator;


    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    #region Player
    public void GolpeConBate()
    {
        animator.SetTrigger("GolpeBate");
    }
    public void GolpeConBoom()
    {
        animator.SetTrigger("GolpeBoom");
    }
    public void Salto()
    {
        animator.SetTrigger(AnimTags.Salto_Tag);
            
    }
    public void Ensalto(bool pEnsalto)
    {
        animator.SetBool(AnimTags.Ensalto_Tag, pEnsalto);
    }
    public void PatadaEnAire()
    {
        animator.SetTrigger(AnimTags.PatadaEnElAire_Trigger);
    }
    public void Habilidad()
    {
        animator.SetTrigger("Habilidad");
    }
    public void Correr(bool pCorrer)
    {
        animator.SetBool(AnimTags.Movimiento, pCorrer);
    }
     public void PunchR()
    {
        animator.SetTrigger(AnimTags.Golpe1_Trigger);
    }
    public void PunchL()
    {
        animator.SetTrigger(AnimTags.Golpe2_Trigger);
    }
    public void Uppercut()
    {
        animator.SetTrigger(AnimTags.Golpe3_Trigger);
    }
    public void Patada1()
    {
        animator.SetTrigger(AnimTags.Patada1_Trigger);
    }
    public void Patada2()
    {
        animator.SetTrigger(AnimTags.Patada2_Trigger);
    }
    #endregion

    #region Enemigo
    public void AtaqueDelEnemigo(int ataque)
    {
        if (ataque == 0)
        {
            animator.SetTrigger(AnimTags.Golpe1_Trigger);
        }
        if (ataque == 1)
        {
            animator.SetTrigger(AnimTags.Golpe2_Trigger);
        }
        //if (ataque == 2)
        //{
        //    animator.SetTrigger(AnimTags.Golpe3_Trigger);
        //}
    }

    public void Play_IdleAnimacion()
    {
        animator.Play(AnimTags.IDLE);
    }

    public void Noqueado()
    {
        animator.SetTrigger(AnimTags.Noqueado_Trigger);
    }
    public void Levantarse()
    {
        animator.SetTrigger(AnimTags.Levantarse_Trigger);
    }
    public void Golpeado()
    {
        animator.SetTrigger(AnimTags.Golpeado_Trigger);
    }
    public void Muerto()
    {
        animator.SetTrigger(AnimTags.Death_Trigger);
    }






    #endregion 

}
