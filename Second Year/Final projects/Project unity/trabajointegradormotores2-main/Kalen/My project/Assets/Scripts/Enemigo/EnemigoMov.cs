﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class EnemigoMov : MonoBehaviour
{
    internal AnimacionController EnemigoAnimacion;
    internal Rigidbody rb;

    public float velocidad = 1.8f;

    internal Transform PlayerTarget;
    internal Transform PlayerTarget2;

    public float DistanciaDelAtaque = 0.8f;
    private float PerseguirAlJugadorDespuesDelAtaque = 1f;

    internal float ContadorActual;
    internal float ContadorDefault = 2f;

    internal bool SeguirJugador, AtacarJugador;

    private Quaternion angulo;
    private float grado;

    private float ContadorDeRutina = 0;
    public int ContadorDeGolpes = 0;
    private int Mov = 0;
    private float contan;

    public int maxgolpes;
    public float tiempoafk;

    public int Ram;


   


    void Awake()
    {
        EnemigoAnimacion = GetComponentInChildren<AnimacionController>();
        rb = GetComponent<Rigidbody>();
        PlayerTarget = GameObject.FindWithTag(Tags.Jugador_Tag).transform;
        PlayerTarget2 = GameObject.FindWithTag("Jugador2").transform;
        
    }
    void Start()
    {
        ContadorActual = ContadorDefault;
        SeguirJugador = true;
        Ram = Random.Range(0,1);
    }
    void FixedUpdate()
    {
        if (Ram == 0)
        {
            seguirjugador();
           
        }
        if (Ram == 1)
        {
            SeguirJugador2();


        }

    }
    void Update()
    {

        if (ContadorDeGolpes >= maxgolpes)
        {

            contan += 1 * Time.deltaTime;

            if (contan >= 0.1f)
            {
                Mov = Random.Range(0, 1);
                Random.Range(0, 1);
                contan = 0;
            }
            switch (Mov)
            {
                case 0:
                    grado = Random.Range(0, 360);

                    angulo = Quaternion.Euler(0, grado, 0);
                    Mov++;
                    break;
                case 1:
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, angulo, 0.5f);
                    rb.velocity = transform.forward * velocidad;
                    if (rb.velocity.sqrMagnitude != 0) //RaizCuadrada, No le estamos dando ninguna velocidad 
                    {
                        EnemigoAnimacion.Correr(true);

                    }
                    ContadorDeRutina += 1 * Time.deltaTime;
                    if (ContadorDeRutina >= tiempoafk)
                    {
                        ContadorDeRutina = 0;
                        ContadorDeGolpes = 0;
                    }
                    break;
                default:
                    break;
            }





        }
        else
        {
            if (Ram == 0)
            {
                Atacar();
            }
            
        }
        if (ContadorDeGolpes >= maxgolpes)
        {
            contan += 1 * Time.deltaTime;

            if (contan >= 0.5f)
            {
                Mov = Random.Range(0, 1);
                Ram = Random.Range(0, 1);
                contan = 0;
            }
            switch (Mov)
            {
                case 0:
                    grado = Random.Range(0, 360);
                    angulo = Quaternion.Euler(0, grado, 0);
                    Mov++;
                    break;
                case 1:
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, angulo, 0.5f);
                    rb.velocity = transform.forward * velocidad;
                    if (rb.velocity.sqrMagnitude != 0) //RaizCuadrada, No le estamos dando ninguna velocidad 
                    {
                        EnemigoAnimacion.Correr(true);

                    }
                    ContadorDeRutina += 1 * Time.deltaTime;
                    if (ContadorDeRutina >= tiempoafk)
                    {
                        ContadorDeRutina = 0;
                        ContadorDeGolpes = 0;
                    }
                    break;
                default:
                    break;
            }





        }
        else 
        {
            if (Ram == 1)
            {

            }
            Atacar2();
        }
        
        
    }
    void SeguirJugador2()
    {
        if (!SeguirJugador)
        {
            return;
        }
        if (PlayerTarget2.CompareTag("Jugador2"))
        {
            float posx = PlayerTarget2.position.x;
            float posz = PlayerTarget2.position.z;

            Vector3 xz = new Vector3(posx, 1, posz);
            if (ContadorDeGolpes < maxgolpes)
            {
                if (Vector3.Distance(transform.position, PlayerTarget2.position) > DistanciaDelAtaque)
                {
                   
                    transform.LookAt(xz); //Solo mira la posicion xz no mira la y entonces no se gira el enmigo

                    rb.velocity = transform.forward * velocidad;
                    if (rb.velocity.sqrMagnitude != 0) //RaizCuadrada, No le estamos dando ninguna velocidad 
                    {
                        EnemigoAnimacion.Correr(true);

                    }

                    if (ContadorDeRutina >= 5f)
                    {
                        ContadorDeRutina = 0;
                    }
                }
                else if (Vector3.Distance(transform.position, PlayerTarget2.position) <= DistanciaDelAtaque)
                {
                    rb.velocity = Vector3.zero; //DetieneElmovimiento
                    EnemigoAnimacion.Correr(false);
                    SeguirJugador = false;
                    AtacarJugador = true;

                }
                else
                {
                    EnemigoAnimacion.Correr(false);
                    AtacarJugador = false;
                }

            }

       
        }

    }
    void seguirjugador()
    {
        if (!SeguirJugador)
        {
            return;
        }
        if (PlayerTarget.CompareTag("Jugador"))
        {
            float posx = PlayerTarget.position.x;
            float posz = PlayerTarget.position.z;

            Vector3 xz = new Vector3(posx, 1, posz);
            transform.LookAt(xz); //Solo mira la posicion xz no mira la y entonces no se gira el enmigo
            if (ContadorDeGolpes<maxgolpes)
            {

                if (Vector3.Distance(transform.position, PlayerTarget.position) > DistanciaDelAtaque)
                {
                    

                    rb.velocity = transform.forward * velocidad;
                    if (rb.velocity.sqrMagnitude != 0) //RaizCuadrada, No le estamos dando ninguna velocidad 
                    {
                        EnemigoAnimacion.Correr(true);

                    }

                    if (ContadorDeRutina >= 5f)
                    {
                        ContadorDeRutina = 0;
                    }
                }
                else if (Vector3.Distance(transform.position, PlayerTarget.position) <= DistanciaDelAtaque)
                {
                    rb.velocity = Vector3.zero; //DetieneElmovimiento
                    EnemigoAnimacion.Correr(false);
                    SeguirJugador = false;
                    AtacarJugador = true;

                }
                else
                {
                    EnemigoAnimacion.Correr(false);
                    AtacarJugador = false;
                }

            }

           
        }
          

       
    }
    
    void Atacar()
    {
        if (!AtacarJugador)
        {
            return;
        }
        transform.LookAt(PlayerTarget);
        ContadorActual += Time.deltaTime;
        if (ContadorActual > ContadorDefault)
        {
            EnemigoAnimacion.AtaqueDelEnemigo(Random.Range(0, 2));
            ContadorActual = 0f;
        }
        if (Vector3.Distance(transform.position, PlayerTarget.position) > DistanciaDelAtaque + PerseguirAlJugadorDespuesDelAtaque)
        {
            AtacarJugador = false;
            SeguirJugador = true;

        }
        
        
        
    }
    void Atacar2()
    {
        if (!AtacarJugador)
        {
            return;
        }
        ContadorActual += Time.deltaTime;
        transform.LookAt(PlayerTarget);
        if (ContadorActual > ContadorDefault)
        {
            EnemigoAnimacion.AtaqueDelEnemigo(Random.Range(0, 2));
            ContadorActual = 0f;
        }
        if (Vector3.Distance(transform.position, PlayerTarget2.position) > DistanciaDelAtaque + PerseguirAlJugadorDespuesDelAtaque)
        {
            AtacarJugador = false;
            SeguirJugador = true;

        }



    }

   


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            ContadorDeGolpes++;
       
        }
      

    }

    









}
