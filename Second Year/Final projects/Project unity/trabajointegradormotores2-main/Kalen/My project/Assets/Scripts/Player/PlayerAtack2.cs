﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAtack2 : MonoBehaviour
{
    public bool conbate = false;
    public bool conbom = false;
    public enum EstadoCombo
    {
        Ninguno,PunchR,PunchL,Uppercut,Patada1,Patada2,
            PatadaEnAire,Habilidad
    }
    private AnimacionController PlayerAnimator;
    private HabilidadUI2 Habilidad_UI;
    private bool ReiniciarCont;
    public GameObject Bate, Boomer;
    public LayerMask capaPiso;
    private CapsuleCollider JugadorCapsuleCollider;
    private float ContadorDefault = 0.4f;
    private float ContadorActual;
    private float BarraDeHabilidad;
    private EstadoCombo Actual_Estado_Combo;
    public int cont = 0;
    void Awake()
    {
        PlayerAnimator = GetComponentInChildren<AnimacionController>();
        JugadorCapsuleCollider = GetComponent<CapsuleCollider>();
        Habilidad_UI = GetComponent<HabilidadUI2>();
    }

    void Start()
    {
        ContadorActual = ContadorDefault;
        Actual_Estado_Combo = EstadoCombo.Ninguno;
    }

    void Update()
    {
        if (BarraDeHabilidad <=100)
        {
            BarraDeHabilidad += 8f * Time.deltaTime;
        }
        
        Habilidad_UI.MostrarVida(BarraDeHabilidad);
        ComboGolpe();
        ReiniciarEstadoDelCombo();
        if (Input.GetButtonDown("JFire2") && !EnElPiso())
        {
            PlayerAnimator.PatadaEnAire();
        }
        if (cont >= 5)
        {
            conbate = false;
        }
        

    }

    void ComboGolpe()
    {
        if (Input.GetButtonDown("JFire1") && !conbate)
        {
            if (Actual_Estado_Combo == EstadoCombo.Uppercut|| Actual_Estado_Combo == EstadoCombo.Patada1|| Actual_Estado_Combo == EstadoCombo.Patada2 || Actual_Estado_Combo == EstadoCombo.Habilidad)
            {
                return; //Poner un return dentro de un metodo void hace que salga directamente del metodo
            }
            Actual_Estado_Combo++;
            ReiniciarCont = true;
            ContadorActual = ContadorDefault;

            if (Actual_Estado_Combo == EstadoCombo.PunchR && EnElPiso())
            {
                PlayerAnimator.PunchR();
            }
            if (Actual_Estado_Combo == EstadoCombo.PunchL && EnElPiso())
            {
                PlayerAnimator.PunchL();
            }
            if (Actual_Estado_Combo == EstadoCombo.Uppercut && EnElPiso())
            {
                PlayerAnimator.Uppercut();
            }

        }
        else if (Input.GetButtonDown("JFire1") && conbate && EnElPiso())
        {
            PlayerAnimator.GolpeConBate();
        }
        if (Input.GetButtonDown("JFire1") && conbom && EnElPiso())
        {
            PlayerAnimator.GolpeConBoom();

        }
        if (Input.GetButtonDown("JFire2"))
        {
            // si El estado del combo es golpe 3 o patada 2, sale del combo ya que no puede seguir
            if (Actual_Estado_Combo == EstadoCombo.Patada2|| Actual_Estado_Combo == EstadoCombo.Uppercut)
            {
                return;
            }
            if (Actual_Estado_Combo == EstadoCombo.Ninguno || Actual_Estado_Combo == EstadoCombo.PunchR|| Actual_Estado_Combo == EstadoCombo.PunchL && EnElPiso())
            {
                Actual_Estado_Combo = EstadoCombo.Patada1;
            }
            else if (Actual_Estado_Combo == EstadoCombo.Patada1 && EnElPiso())
            {
                Actual_Estado_Combo++;
            }
           

            ReiniciarCont = true;
            ContadorActual = ContadorDefault;

            if (Actual_Estado_Combo == EstadoCombo.Patada1 && EnElPiso())
            {
                PlayerAnimator.Patada1();
            }
            if (Actual_Estado_Combo == EstadoCombo.Patada2 && EnElPiso())
            {
                PlayerAnimator.Patada2();
            }
           




        }
        if (Input.GetButtonDown("JFire3") && BarraDeHabilidad >= 100f)
        {
            if (Actual_Estado_Combo == EstadoCombo.Patada2 || Actual_Estado_Combo == EstadoCombo.Uppercut || Actual_Estado_Combo == EstadoCombo.Habilidad)
            {
                return;
            }
            if (Actual_Estado_Combo == EstadoCombo.Ninguno)
            {
                Actual_Estado_Combo = EstadoCombo.Habilidad;
            }
            else if (Actual_Estado_Combo == EstadoCombo.Habilidad)
            {
                Actual_Estado_Combo++;
            }

            ReiniciarCont = true;
            ContadorActual = ContadorDefault;

            if (Actual_Estado_Combo == EstadoCombo.Habilidad)
            {
                PlayerAnimator.Habilidad();
            }


            BarraDeHabilidad = 0;

        }
      

       
    }



    void ReiniciarEstadoDelCombo()
    {
        if (ReiniciarCont)
        {
            ContadorActual -= Time.deltaTime;
            if (ContadorActual <= 0f)
            {
                Actual_Estado_Combo = EstadoCombo.Ninguno;
                ReiniciarCont = false;
                ContadorActual = ContadorDefault;
            }
        }
    }



    public bool EnElPiso()
    {
        return Physics.CheckCapsule(JugadorCapsuleCollider.bounds.center, new Vector3(JugadorCapsuleCollider.bounds.center.x,
        JugadorCapsuleCollider.bounds.min.y, JugadorCapsuleCollider.bounds.center.z), JugadorCapsuleCollider.radius * .9f, capaPiso);
        

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bate"))
        {
            Bate.SetActive(true);
            other.gameObject.SetActive(false);
            conbate = true;
            cont = 0;
        }
        if (other.gameObject.CompareTag("Boomerang"))
        {
            Boomer.SetActive(true);
            other.gameObject.SetActive(false);
            conbom = true;
        }

    }
    

























}
