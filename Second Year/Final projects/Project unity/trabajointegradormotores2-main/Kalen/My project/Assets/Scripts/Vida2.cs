﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vida2 : MonoBehaviour
{

    public float Vida_ = 100f;

    private AnimacionController AnimacionScript;
    //private PlayerMov MovimientoPlayer;


    private bool PjMurio;
    public bool Es_Jugador,Es_Enemigo;

    private VidaUI2 Vida_UI;

    

     void Awake()
    {
        AnimacionScript = GetComponentInChildren<AnimacionController>();
        if (Es_Jugador)
        {
            Vida_UI = GetComponent<VidaUI2>();
        }
       
    }
    public void AplicarDaño(float Daño,bool Noqueado)
    {
        if (PjMurio)
        {
            return;
        }
        Vida_ -= Daño;
        if (Es_Jugador)
        {
            Vida_UI.MostrarVida(Vida_);
        }

        if (Vida_ <= 0f)
        {
            AnimacionScript.Muerto();
            PjMurio = true;
           
            if (Es_Jugador)
            {
                GameObject.FindWithTag(Tags.Enemigo_Tag).GetComponent<EnemigoMov>().enabled = false;
            }
            return;
        }
        if (!Es_Jugador)
        {
            if (Noqueado)
            {
                if (Random.Range(0,2) > 0)
                {
                    AnimacionScript.Noqueado();
                }

            }
            else
            {
                if (Random.Range(0, 3) > 1)
                {
                    AnimacionScript.Golpeado();
                }
            }
        }

    
       

       









    }


 
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Daño") && Es_Jugador)
        {
            AplicarDaño(3 * Time.deltaTime,false);
        }
        
        

        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Pro"))
        {
            AplicarDaño(50, true);
        }
        if (collision.gameObject.CompareTag("Jefe") && Es_Jugador)
        {
            AplicarDaño(20, false);
        }
       
       
    }
}

