﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boomerang : MonoBehaviour
{
    bool va;
    GameObject Player;
    GameObject Boomerangg;
    Transform BoomRotacion;
    Vector3 ItemEnfrentedepj;
    void Start()
    {
        va = false;
        Player = GameObject.Find("Jugador");
        Boomerangg = GameObject.Find("Boomerang");
        Boomerangg.GetComponent<MeshRenderer>().enabled = false;
        BoomRotacion = gameObject.transform.GetChild(0);
        ItemEnfrentedepj = new Vector3(Player.transform.position.x, Player.transform.position.y + 1, transform.position.z) + Player.transform.forward * 10f;
        StartCoroutine(Boom());
    }

   IEnumerator Boom()
    {
        va = true;
        yield return new WaitForSecondsRealtime(1.5f);
        va = false;
    }
    void Update()
    {
        BoomRotacion.transform.Rotate(0, Time.deltaTime * 500, 0);
        if (va)
        {
            transform.position = Vector3.MoveTowards(transform.position, ItemEnfrentedepj, Time.deltaTime * 40);

        }
        if (!va)
        {
            transform.position = Vector3.MoveTowards(transform.position,new Vector3(Player.transform.position.x,Player.transform.position.y + 1,Player.transform.position.z), Time.deltaTime * 40);
        }
        if (!va && Vector3.Distance(Player.transform.position,transform.position)<1.5)
        {
            Boomerangg.GetComponent<MeshRenderer>().enabled = true;
            Destroy(this.gameObject);
        }
    }
}
