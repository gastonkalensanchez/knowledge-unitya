﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class VidaUI : MonoBehaviour
{
    private Image Vida_UI;
    // Start is called before the first frame update
    void Awake()
    {
        Vida_UI = GameObject.FindWithTag(Tags.Vida_UI).GetComponent<Image>();
    }
    

    
    

    public void MostrarVida(float valor)
    {
        valor /= 100;
        if (valor < 0f)
        {
            valor = 0f;
        }
        Vida_UI.fillAmount = valor;
    }

}
