﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HabilidadUI : MonoBehaviour
{
    private Image Habilidad_UI;
    // Start is called before the first frame update
    void Awake()
    {
        Habilidad_UI = GameObject.FindWithTag("HabilidadUI").GetComponent<Image>();
    }





    public void MostrarVida(float valor)
    {
        valor /= 100;
        if (valor < 0f)
        {
            valor = 0f;
        }
        Habilidad_UI.fillAmount = valor;
    }

}
