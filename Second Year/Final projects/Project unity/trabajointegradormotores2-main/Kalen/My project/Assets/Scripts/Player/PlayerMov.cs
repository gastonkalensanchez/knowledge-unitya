﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMov : MonoBehaviour
{
    Rigidbody rb;
    public float Velocidad = 3.5f;
    public float Zvelocidad = 4f;

    private float RotacionY = -90;
    //private float VelocidadRotacion = 15f;

    private AnimacionController PlayerAnimator;
    private PlayerAtack AtaqueJugador;
   
    void Start()
    {
        PlayerAnimator = GetComponentInChildren<AnimacionController>(); //Obtener codigo del hijo de jugador
        AtaqueJugador = GetComponent<PlayerAtack>();
        
    }
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
        
    void FixedUpdate()
    {
        DetectarMovimiento();
    }
   
    void Update()
    {
        Rotacion();
        AnimacionDeCorrer_Jugador();
        SaltoJugador();
    }

    void DetectarMovimiento()
    {
        rb.velocity = new Vector3(Input.GetAxisRaw(Axis.Horizontal_Axis)
            * (Velocidad),rb.velocity.y,Input.GetAxisRaw(Axis.Vertical_Axis)*(Zvelocidad));


    }
    void Rotacion()
    {
        if (Input.GetAxisRaw(Axis.Horizontal_Axis) > 0)
        {
            transform.rotation = Quaternion.Euler(0f, Mathf.Abs(RotacionY), 0f);
        }
        if (Input.GetAxisRaw(Axis.Horizontal_Axis) < 0)
        {
            transform.rotation = Quaternion.Euler(0f,RotacionY, 0f);
        }
    }



    void SaltoJugador()
    {
        if (Input.GetKeyDown(KeyCode.Space) && AtaqueJugador.EnElPiso())
        {
            PlayerAnimator.Salto();
            rb.AddForce(transform.up * 5, ForceMode.Impulse);
            
        }
        if (AtaqueJugador.EnElPiso())
        {
            PlayerAnimator.Ensalto(false);
        }
        if (!AtaqueJugador.EnElPiso())
        {
            PlayerAnimator.Ensalto(true);
        }
    }

    void AnimacionDeCorrer_Jugador()
    {
        if (Input.GetAxisRaw(Axis.Horizontal_Axis) != 0 || Input.GetAxisRaw(Axis.Vertical_Axis) != 0)
        {
            PlayerAnimator.Correr(true);
        }
        else
        {
            PlayerAnimator.Correr(false);
        }
    }

   
}
