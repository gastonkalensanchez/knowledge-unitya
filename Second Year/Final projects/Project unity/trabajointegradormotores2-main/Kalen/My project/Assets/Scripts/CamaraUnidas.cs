﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraUnidas : MonoBehaviour
{
    public Camera cam;
   
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

    
    }
    private void OnTriggerStay(Collider other)
    {

        if (other.gameObject.CompareTag("aia"))
        {
            cam.rect = new Rect(0, 0, 1, 1);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("aia"))
        {
            cam.rect = new Rect(0, 0.5f, 1, 1);
        }
    }
    
}
