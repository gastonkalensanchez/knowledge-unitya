﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniversalAtack : MonoBehaviour
{
    public LayerMask CollisionLayer;
    public float Radio = 1f;
    public float Daño = 2f;
    public int cont = 0;
    public bool Es_Jugador, Es_Enemigo,Bate,Boom;

    public GameObject ParticulasGolpe;
    public GameObject Bate_,Boomm;
    public PlayerAtack AtaqueDelJugador;
    

    void Update()
    {
        DetectarColision();
    }

    void DetectarColision()
    {
        Collider[] Golpeado = Physics.OverlapSphere(transform.position, Radio, CollisionLayer);
        if (Golpeado.Length > 0)
        {
            if (Es_Jugador)
            {
                Vector3 ParticulasGolpe_Posicion = Golpeado[0].transform.position;
                ParticulasGolpe_Posicion.y += 0.3f;
                if (Golpeado[0].transform.forward.x > 0)
                {
                    ParticulasGolpe_Posicion.x += 0.1f;
                }
                else if (Golpeado[0].transform.forward.x < 0)
                {
                    ParticulasGolpe_Posicion.x -= 0.1f;
                }
                Instantiate(ParticulasGolpe, ParticulasGolpe_Posicion, Quaternion.identity);
                if (gameObject.CompareTag(Tags.ManoR_Tag)||gameObject.CompareTag(Tags.PiernaR_Tag))
                {
                    Golpeado[0].GetComponent<Vida>().AplicarDaño(Daño, true);
                }
                else
                {
                    Golpeado[0].GetComponent<Vida>().AplicarDaño(Daño, false);
                }


            }//Si es jugador

            if (Es_Enemigo)
            {
                Golpeado[0].GetComponent<Vida>().AplicarDaño(Daño, false);
            }
           
            
            gameObject.SetActive(false);
            cont++;
            if (Bate && cont == 5)
            {
                Bate_.SetActive(false);
                cont = 0;
                AtaqueDelJugador.conbate = false;
               
            }
            if (Boom && cont == 5)
            {
                Boomm.SetActive(false);
                cont = 0;
                AtaqueDelJugador.conbom = false;

            }




        }

       
    }    //2:27:52 












}
