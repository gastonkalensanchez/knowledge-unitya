﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacionControllerDelegate : MonoBehaviour
{

    public GameObject ProyectilPj;
    public Transform PuntoDeSalidaPj;

    public GameObject Mano_Derecha_Punto, Mano_Izquierda_Punto, Pie_Derecho_Punto,Bate_Punto, BoomPunto;

    internal bool EnemigoNoqueado;


    private AnimacionController AnimationScript;

    private EnemigoMov MovimientoDelEnemigo;
   
    private PlayerMov MovimientoDelJugador;
    private PlayerAtack AtaqueDelJugador;
    public GameObject Jugador;
    private Rigidbody rb;

    void Awake()
    {
      
        AnimationScript = GetComponent<AnimacionController>();
        if (gameObject.CompareTag(Tags.Enemigo_Tag))
        {
            MovimientoDelEnemigo = GetComponentInParent<EnemigoMov>();
            rb = GetComponentInParent<Rigidbody>();

        }
        if (gameObject.CompareTag(Tags.Jugador_Tag))
        {
            MovimientoDelJugador = GetComponentInParent<PlayerMov>();
            AtaqueDelJugador = GetComponentInParent<PlayerAtack>();
        }
       
        
    }
 
    void ProyectilPersonaje()
    {
        GameObject pro;
        pro = Instantiate(ProyectilPj, PuntoDeSalidaPj.transform.position, transform.rotation);

        Rigidbody rb = pro.GetComponent<Rigidbody>();
        rb.AddForce(PuntoDeSalidaPj.transform.forward * 15, ForceMode.Impulse);

        Destroy(pro, 5);


    }
    void Bate_PuntoON()
    {
        Bate_Punto.SetActive(true);
    }
    void Bate_PuntoOFF()
    {
        Bate_Punto.SetActive(false);
    }
    void Boom_PuntoON()
    {
        BoomPunto.SetActive(true);
    }
    void Boom_PuntoOFF()
    {
        BoomPunto.SetActive(false);
    }
    void Mano_Derecha_PuntoON()
    {
        Mano_Derecha_Punto.SetActive(true);
    }
    void Mano_Derecha_PuntoOFF()
    {
        if (Mano_Derecha_Punto.activeInHierarchy)
        {
            Mano_Derecha_Punto.SetActive(false);
        }
        
    }

    void Mano_Izquierda_PuntoON()
    {
        Mano_Izquierda_Punto.SetActive(true);
    }
    void Mano_Izquierda_PuntoOFF()
    {
        if (Mano_Izquierda_Punto.activeInHierarchy)
        {
            Mano_Izquierda_Punto.SetActive(false);
        }
    }

    void Pie_Derecho_PuntoON()
    {
        Pie_Derecho_Punto.SetActive(true);
    }

    void Pie_Derecho_PuntoOFF()
    {
        if (Pie_Derecho_Punto.activeInHierarchy)
        {
            Pie_Derecho_Punto.SetActive(false);
        }
    }

    void TagManoDerecha()
    {

        Mano_Derecha_Punto.tag = Tags.ManoR_Tag;
    }
    void unTagManoDerecha()
    {
        Mano_Derecha_Punto.tag = Tags.Untagged_Tag;
    }
    void TagPiernaDerecha()
    {

        Pie_Derecho_Punto.tag = Tags.PiernaR_Tag;
    }
    void unTagPiernaDerecha()
    {
        Pie_Derecho_Punto.tag = Tags.Untagged_Tag;
    }

    void EnemigoLevantando()
    {
        StartCoroutine(LevantarseDespuesDeUntiempo());
    }


    IEnumerator LevantarseDespuesDeUntiempo()
    {
        yield return new WaitForSeconds(0.5f);
        AnimationScript.Levantarse();
    }
    void PrenderMovimientoEnemigo()
    {
        MovimientoDelEnemigo.enabled = true;
        MovimientoDelEnemigo.Ram = Random.Range(0, 1);
        
        rb.constraints = RigidbodyConstraints.None;
        rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
        EnemigoNoqueado = false;
        
        
        //pone el layer enemigo
        transform.parent.gameObject.layer = 9;
    }
    void CancelarMovimientoEnemigo()
    {
        EnemigoNoqueado = true;
        rb.constraints = RigidbodyConstraints.FreezeAll;
        MovimientoDelEnemigo.enabled = false;


        //MovimientoPlayer.enabled = false;
        //pone el leyer en default
        transform.parent.gameObject.layer = 0;
    }
    void PrenderMovimientoJugador()
    {
        MovimientoDelJugador.enabled = true;
        
    }
    void CancelarMovimientoJugador()
    {
        MovimientoDelJugador.enabled = false;
       
    }
    void PrenderMovimientoJugadorH()
    {
        MovimientoDelJugador.enabled = true;
        AtaqueDelJugador.enabled = true;
        Jugador.tag = "Jugador";
        gameObject.tag = "Jugador";
    }
    void CancelarMovimientoJugadorH()
    {
        MovimientoDelJugador.enabled = false;
        AtaqueDelJugador.enabled = false;       
        Jugador.tag = "Nada";
        gameObject.tag = "Nada";
    }

    void Muerte()
    {
        Invoke("DesactivarGameObjet", 2f);
    }
    void DesactivarGameObjet()
    {
        gameObject.SetActive(false);
    }
  
}
