﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 (void);
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 (void);
// 0x00000005 System.Exception System.Linq.Error::NotSupported()
extern void Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D (void);
// 0x00000006 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000008 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000009 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x0000000A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000C System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000D System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000E System.Collections.Generic.IEnumerable`1<System.Linq.IGrouping`2<TKey,TSource>> System.Linq.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000010 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DistinctIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000011 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Except(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000012 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ExceptIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000013 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Reverse(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000014 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ReverseIterator(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000015 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000016 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000017 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000018 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000019 System.Linq.ILookup`2<TKey,TSource> System.Linq.Enumerable::ToLookup(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000001A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x0000001B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x0000001C TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001D TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001E TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001F TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000020 TSource System.Linq.Enumerable::LastOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000021 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000022 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000023 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000024 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000025 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000026 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000027 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000028 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000029 System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583 (void);
// 0x0000002A System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Int32>)
// 0x0000002B System.Int32 System.Linq.Enumerable::Max(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Max_m841FC5439B3D8021B3E0D782522FDD40CFA29D88 (void);
// 0x0000002C System.Int32 System.Linq.Enumerable::Max(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Int32>)
// 0x0000002D System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000002E TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000002F System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000030 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000031 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000032 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000033 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000034 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000035 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000036 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000037 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000038 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000039 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000003A System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000003B System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000003C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000003D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000003E System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000003F System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000040 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000041 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000042 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000043 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000044 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000045 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000046 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000047 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000048 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000049 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x0000004A System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000004B System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000004C System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000004D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000004E System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000004F System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000050 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000051 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000052 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000053 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000054 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000055 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000056 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000057 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000058 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000059 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000005A System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x0000005B TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000005C System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x0000005D System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x0000005E System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__17`2::MoveNext()
// 0x0000005F System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x00000060 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x00000061 TResult System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000062 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x00000063 System.Object System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x00000064 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000065 System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000066 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::.ctor(System.Int32)
// 0x00000067 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.IDisposable.Dispose()
// 0x00000068 System.Boolean System.Linq.Enumerable_<DistinctIterator>d__68`1::MoveNext()
// 0x00000069 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::<>m__Finally1()
// 0x0000006A TSource System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000006B System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.Reset()
// 0x0000006C System.Object System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.get_Current()
// 0x0000006D System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000006E System.Collections.IEnumerator System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000006F System.Void System.Linq.Enumerable_<ExceptIterator>d__77`1::.ctor(System.Int32)
// 0x00000070 System.Void System.Linq.Enumerable_<ExceptIterator>d__77`1::System.IDisposable.Dispose()
// 0x00000071 System.Boolean System.Linq.Enumerable_<ExceptIterator>d__77`1::MoveNext()
// 0x00000072 System.Void System.Linq.Enumerable_<ExceptIterator>d__77`1::<>m__Finally1()
// 0x00000073 TSource System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000074 System.Void System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.IEnumerator.Reset()
// 0x00000075 System.Object System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.IEnumerator.get_Current()
// 0x00000076 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000077 System.Collections.IEnumerator System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000078 System.Void System.Linq.Enumerable_<ReverseIterator>d__79`1::.ctor(System.Int32)
// 0x00000079 System.Void System.Linq.Enumerable_<ReverseIterator>d__79`1::System.IDisposable.Dispose()
// 0x0000007A System.Boolean System.Linq.Enumerable_<ReverseIterator>d__79`1::MoveNext()
// 0x0000007B TSource System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000007C System.Void System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.IEnumerator.Reset()
// 0x0000007D System.Object System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.IEnumerator.get_Current()
// 0x0000007E System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000007F System.Collections.IEnumerator System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000080 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x00000081 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x00000082 System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x00000083 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x00000084 TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000085 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x00000086 System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x00000087 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000088 System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000089 System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x0000008A System.Void System.Linq.IdentityFunction`1_<>c::.cctor()
// 0x0000008B System.Void System.Linq.IdentityFunction`1_<>c::.ctor()
// 0x0000008C TElement System.Linq.IdentityFunction`1_<>c::<get_Instance>b__1_0(TElement)
// 0x0000008D System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000008E System.Linq.Lookup`2<TKey,TElement> System.Linq.Lookup`2::Create(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x0000008F System.Void System.Linq.Lookup`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000090 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.Lookup`2::GetEnumerator()
// 0x00000091 System.Collections.IEnumerator System.Linq.Lookup`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000092 System.Int32 System.Linq.Lookup`2::InternalGetHashCode(TKey)
// 0x00000093 System.Linq.Lookup`2_Grouping<TKey,TElement> System.Linq.Lookup`2::GetGrouping(TKey,System.Boolean)
// 0x00000094 System.Void System.Linq.Lookup`2::Resize()
// 0x00000095 System.Void System.Linq.Lookup`2_Grouping::Add(TElement)
// 0x00000096 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.Lookup`2_Grouping::GetEnumerator()
// 0x00000097 System.Collections.IEnumerator System.Linq.Lookup`2_Grouping::System.Collections.IEnumerable.GetEnumerator()
// 0x00000098 System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_Count()
// 0x00000099 System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_IsReadOnly()
// 0x0000009A System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x0000009B System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x0000009C System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Contains(TElement)
// 0x0000009D System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.CopyTo(TElement[],System.Int32)
// 0x0000009E System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x0000009F System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.IndexOf(TElement)
// 0x000000A0 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x000000A1 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x000000A2 TElement System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x000000A3 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x000000A4 System.Void System.Linq.Lookup`2_Grouping::.ctor()
// 0x000000A5 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::.ctor(System.Int32)
// 0x000000A6 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.IDisposable.Dispose()
// 0x000000A7 System.Boolean System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::MoveNext()
// 0x000000A8 TElement System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000A9 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.Reset()
// 0x000000AA System.Object System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.get_Current()
// 0x000000AB System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::.ctor(System.Int32)
// 0x000000AC System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.IDisposable.Dispose()
// 0x000000AD System.Boolean System.Linq.Lookup`2_<GetEnumerator>d__12::MoveNext()
// 0x000000AE System.Linq.IGrouping`2<TKey,TElement> System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.Generic.IEnumerator<System.Linq.IGrouping<TKey,TElement>>.get_Current()
// 0x000000AF System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.Reset()
// 0x000000B0 System.Object System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.get_Current()
// 0x000000B1 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x000000B2 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x000000B3 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x000000B4 System.Void System.Linq.Set`1::Resize()
// 0x000000B5 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x000000B6 System.Void System.Linq.GroupedEnumerable`3::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000B7 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.GroupedEnumerable`3::GetEnumerator()
// 0x000000B8 System.Collections.IEnumerator System.Linq.GroupedEnumerable`3::System.Collections.IEnumerable.GetEnumerator()
// 0x000000B9 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x000000BA System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000BB System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000BC System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000BD System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x000000BE System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x000000BF System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x000000C0 System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x000000C1 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000C2 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x000000C3 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000C4 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000C5 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000C6 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000C7 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000C8 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000C9 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000CA System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000CB System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000CC System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000CD System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x000000CE System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000CF TElement[] System.Linq.Buffer`1::ToArray()
// 0x000000D0 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x000000D1 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000D2 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000D3 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000D4 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000D5 System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x000000D6 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x000000D7 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x000000D8 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x000000D9 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x000000DA System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x000000DB System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x000000DC System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x000000DD System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x000000DE System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000DF System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000E0 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000E1 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x000000E2 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x000000E3 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000E4 System.Void System.Collections.Generic.HashSet`1::ExceptWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000E5 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x000000E6 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000E7 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x000000E8 System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x000000E9 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000EA System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000EB System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000EC System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000ED System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x000000EE System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x000000EF System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000F0 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000F1 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x000000F2 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000F3 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000F4 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000F5 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[245] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583,
	NULL,
	Enumerable_Max_m841FC5439B3D8021B3E0D782522FDD40CFA29D88,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[245] = 
{
	0,
	0,
	4,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	95,
	-1,
	95,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[74] = 
{
	{ 0x02000004, { 111, 4 } },
	{ 0x02000005, { 115, 9 } },
	{ 0x02000006, { 126, 7 } },
	{ 0x02000007, { 135, 10 } },
	{ 0x02000008, { 147, 11 } },
	{ 0x02000009, { 161, 9 } },
	{ 0x0200000A, { 173, 12 } },
	{ 0x0200000B, { 188, 1 } },
	{ 0x0200000C, { 189, 2 } },
	{ 0x0200000D, { 191, 12 } },
	{ 0x0200000E, { 203, 11 } },
	{ 0x0200000F, { 214, 11 } },
	{ 0x02000010, { 225, 6 } },
	{ 0x02000011, { 231, 6 } },
	{ 0x02000012, { 237, 4 } },
	{ 0x02000013, { 241, 3 } },
	{ 0x02000017, { 244, 17 } },
	{ 0x02000018, { 265, 5 } },
	{ 0x02000019, { 270, 1 } },
	{ 0x0200001B, { 271, 8 } },
	{ 0x0200001D, { 279, 4 } },
	{ 0x0200001E, { 283, 3 } },
	{ 0x0200001F, { 288, 5 } },
	{ 0x02000020, { 293, 7 } },
	{ 0x02000021, { 300, 3 } },
	{ 0x02000022, { 303, 7 } },
	{ 0x02000023, { 310, 4 } },
	{ 0x02000024, { 314, 36 } },
	{ 0x02000026, { 350, 2 } },
	{ 0x06000006, { 0, 10 } },
	{ 0x06000007, { 10, 10 } },
	{ 0x06000008, { 20, 5 } },
	{ 0x06000009, { 25, 5 } },
	{ 0x0600000A, { 30, 1 } },
	{ 0x0600000B, { 31, 2 } },
	{ 0x0600000C, { 33, 2 } },
	{ 0x0600000D, { 35, 1 } },
	{ 0x0600000E, { 36, 4 } },
	{ 0x0600000F, { 40, 1 } },
	{ 0x06000010, { 41, 2 } },
	{ 0x06000011, { 43, 1 } },
	{ 0x06000012, { 44, 2 } },
	{ 0x06000013, { 46, 1 } },
	{ 0x06000014, { 47, 2 } },
	{ 0x06000015, { 49, 1 } },
	{ 0x06000016, { 50, 5 } },
	{ 0x06000017, { 55, 3 } },
	{ 0x06000018, { 58, 2 } },
	{ 0x06000019, { 60, 4 } },
	{ 0x0600001A, { 64, 2 } },
	{ 0x0600001B, { 66, 2 } },
	{ 0x0600001C, { 68, 4 } },
	{ 0x0600001D, { 72, 4 } },
	{ 0x0600001E, { 76, 3 } },
	{ 0x0600001F, { 79, 4 } },
	{ 0x06000020, { 83, 4 } },
	{ 0x06000021, { 87, 3 } },
	{ 0x06000022, { 90, 3 } },
	{ 0x06000023, { 93, 1 } },
	{ 0x06000024, { 94, 3 } },
	{ 0x06000025, { 97, 2 } },
	{ 0x06000026, { 99, 3 } },
	{ 0x06000027, { 102, 2 } },
	{ 0x06000028, { 104, 5 } },
	{ 0x0600002A, { 109, 1 } },
	{ 0x0600002C, { 110, 1 } },
	{ 0x0600003C, { 124, 2 } },
	{ 0x06000041, { 133, 2 } },
	{ 0x06000046, { 145, 2 } },
	{ 0x0600004C, { 158, 3 } },
	{ 0x06000051, { 170, 3 } },
	{ 0x06000056, { 185, 3 } },
	{ 0x0600008E, { 261, 4 } },
	{ 0x060000BC, { 286, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[352] = 
{
	{ (Il2CppRGCTXDataType)2, 28514 },
	{ (Il2CppRGCTXDataType)3, 26693 },
	{ (Il2CppRGCTXDataType)2, 28515 },
	{ (Il2CppRGCTXDataType)2, 28516 },
	{ (Il2CppRGCTXDataType)3, 26694 },
	{ (Il2CppRGCTXDataType)2, 28517 },
	{ (Il2CppRGCTXDataType)2, 28518 },
	{ (Il2CppRGCTXDataType)3, 26695 },
	{ (Il2CppRGCTXDataType)2, 28519 },
	{ (Il2CppRGCTXDataType)3, 26696 },
	{ (Il2CppRGCTXDataType)2, 28520 },
	{ (Il2CppRGCTXDataType)3, 26697 },
	{ (Il2CppRGCTXDataType)2, 28521 },
	{ (Il2CppRGCTXDataType)2, 28522 },
	{ (Il2CppRGCTXDataType)3, 26698 },
	{ (Il2CppRGCTXDataType)2, 28523 },
	{ (Il2CppRGCTXDataType)2, 28524 },
	{ (Il2CppRGCTXDataType)3, 26699 },
	{ (Il2CppRGCTXDataType)2, 28525 },
	{ (Il2CppRGCTXDataType)3, 26700 },
	{ (Il2CppRGCTXDataType)2, 28526 },
	{ (Il2CppRGCTXDataType)3, 26701 },
	{ (Il2CppRGCTXDataType)3, 26702 },
	{ (Il2CppRGCTXDataType)2, 21210 },
	{ (Il2CppRGCTXDataType)3, 26703 },
	{ (Il2CppRGCTXDataType)2, 28527 },
	{ (Il2CppRGCTXDataType)3, 26704 },
	{ (Il2CppRGCTXDataType)3, 26705 },
	{ (Il2CppRGCTXDataType)2, 21217 },
	{ (Il2CppRGCTXDataType)3, 26706 },
	{ (Il2CppRGCTXDataType)3, 26707 },
	{ (Il2CppRGCTXDataType)2, 28528 },
	{ (Il2CppRGCTXDataType)3, 26708 },
	{ (Il2CppRGCTXDataType)2, 28529 },
	{ (Il2CppRGCTXDataType)3, 26709 },
	{ (Il2CppRGCTXDataType)3, 26710 },
	{ (Il2CppRGCTXDataType)3, 26711 },
	{ (Il2CppRGCTXDataType)2, 28530 },
	{ (Il2CppRGCTXDataType)2, 28531 },
	{ (Il2CppRGCTXDataType)3, 26712 },
	{ (Il2CppRGCTXDataType)3, 26713 },
	{ (Il2CppRGCTXDataType)2, 28532 },
	{ (Il2CppRGCTXDataType)3, 26714 },
	{ (Il2CppRGCTXDataType)3, 26715 },
	{ (Il2CppRGCTXDataType)2, 28533 },
	{ (Il2CppRGCTXDataType)3, 26716 },
	{ (Il2CppRGCTXDataType)3, 26717 },
	{ (Il2CppRGCTXDataType)2, 28534 },
	{ (Il2CppRGCTXDataType)3, 26718 },
	{ (Il2CppRGCTXDataType)3, 26719 },
	{ (Il2CppRGCTXDataType)3, 26720 },
	{ (Il2CppRGCTXDataType)2, 28535 },
	{ (Il2CppRGCTXDataType)2, 21259 },
	{ (Il2CppRGCTXDataType)2, 28536 },
	{ (Il2CppRGCTXDataType)2, 21261 },
	{ (Il2CppRGCTXDataType)2, 28537 },
	{ (Il2CppRGCTXDataType)3, 26721 },
	{ (Il2CppRGCTXDataType)3, 26722 },
	{ (Il2CppRGCTXDataType)2, 21267 },
	{ (Il2CppRGCTXDataType)3, 26723 },
	{ (Il2CppRGCTXDataType)3, 26724 },
	{ (Il2CppRGCTXDataType)2, 28538 },
	{ (Il2CppRGCTXDataType)3, 26725 },
	{ (Il2CppRGCTXDataType)2, 28539 },
	{ (Il2CppRGCTXDataType)2, 21273 },
	{ (Il2CppRGCTXDataType)3, 26726 },
	{ (Il2CppRGCTXDataType)2, 28540 },
	{ (Il2CppRGCTXDataType)3, 26727 },
	{ (Il2CppRGCTXDataType)2, 28541 },
	{ (Il2CppRGCTXDataType)2, 28542 },
	{ (Il2CppRGCTXDataType)2, 21277 },
	{ (Il2CppRGCTXDataType)2, 28543 },
	{ (Il2CppRGCTXDataType)2, 28544 },
	{ (Il2CppRGCTXDataType)2, 28545 },
	{ (Il2CppRGCTXDataType)2, 21279 },
	{ (Il2CppRGCTXDataType)2, 28546 },
	{ (Il2CppRGCTXDataType)2, 21281 },
	{ (Il2CppRGCTXDataType)2, 28547 },
	{ (Il2CppRGCTXDataType)3, 26728 },
	{ (Il2CppRGCTXDataType)2, 28548 },
	{ (Il2CppRGCTXDataType)2, 28549 },
	{ (Il2CppRGCTXDataType)2, 21284 },
	{ (Il2CppRGCTXDataType)2, 28550 },
	{ (Il2CppRGCTXDataType)2, 28551 },
	{ (Il2CppRGCTXDataType)2, 28552 },
	{ (Il2CppRGCTXDataType)2, 21286 },
	{ (Il2CppRGCTXDataType)2, 28553 },
	{ (Il2CppRGCTXDataType)2, 21288 },
	{ (Il2CppRGCTXDataType)2, 28554 },
	{ (Il2CppRGCTXDataType)3, 26729 },
	{ (Il2CppRGCTXDataType)2, 28555 },
	{ (Il2CppRGCTXDataType)2, 21291 },
	{ (Il2CppRGCTXDataType)2, 28556 },
	{ (Il2CppRGCTXDataType)2, 21293 },
	{ (Il2CppRGCTXDataType)2, 21295 },
	{ (Il2CppRGCTXDataType)2, 28557 },
	{ (Il2CppRGCTXDataType)3, 26730 },
	{ (Il2CppRGCTXDataType)2, 28558 },
	{ (Il2CppRGCTXDataType)2, 21298 },
	{ (Il2CppRGCTXDataType)2, 21300 },
	{ (Il2CppRGCTXDataType)2, 28559 },
	{ (Il2CppRGCTXDataType)3, 26731 },
	{ (Il2CppRGCTXDataType)2, 28560 },
	{ (Il2CppRGCTXDataType)3, 26732 },
	{ (Il2CppRGCTXDataType)3, 26733 },
	{ (Il2CppRGCTXDataType)2, 28561 },
	{ (Il2CppRGCTXDataType)2, 21305 },
	{ (Il2CppRGCTXDataType)2, 28562 },
	{ (Il2CppRGCTXDataType)2, 21307 },
	{ (Il2CppRGCTXDataType)3, 26734 },
	{ (Il2CppRGCTXDataType)3, 26735 },
	{ (Il2CppRGCTXDataType)3, 26736 },
	{ (Il2CppRGCTXDataType)3, 26737 },
	{ (Il2CppRGCTXDataType)2, 21316 },
	{ (Il2CppRGCTXDataType)3, 26738 },
	{ (Il2CppRGCTXDataType)3, 26739 },
	{ (Il2CppRGCTXDataType)2, 21328 },
	{ (Il2CppRGCTXDataType)2, 28563 },
	{ (Il2CppRGCTXDataType)3, 26740 },
	{ (Il2CppRGCTXDataType)3, 26741 },
	{ (Il2CppRGCTXDataType)2, 21330 },
	{ (Il2CppRGCTXDataType)2, 28374 },
	{ (Il2CppRGCTXDataType)3, 26742 },
	{ (Il2CppRGCTXDataType)3, 26743 },
	{ (Il2CppRGCTXDataType)2, 28564 },
	{ (Il2CppRGCTXDataType)3, 26744 },
	{ (Il2CppRGCTXDataType)3, 26745 },
	{ (Il2CppRGCTXDataType)2, 21340 },
	{ (Il2CppRGCTXDataType)2, 28565 },
	{ (Il2CppRGCTXDataType)3, 26746 },
	{ (Il2CppRGCTXDataType)3, 26747 },
	{ (Il2CppRGCTXDataType)3, 25458 },
	{ (Il2CppRGCTXDataType)3, 26748 },
	{ (Il2CppRGCTXDataType)2, 28566 },
	{ (Il2CppRGCTXDataType)3, 26749 },
	{ (Il2CppRGCTXDataType)3, 26750 },
	{ (Il2CppRGCTXDataType)2, 21352 },
	{ (Il2CppRGCTXDataType)2, 28567 },
	{ (Il2CppRGCTXDataType)3, 26751 },
	{ (Il2CppRGCTXDataType)3, 26752 },
	{ (Il2CppRGCTXDataType)3, 26753 },
	{ (Il2CppRGCTXDataType)3, 26754 },
	{ (Il2CppRGCTXDataType)3, 26755 },
	{ (Il2CppRGCTXDataType)3, 25464 },
	{ (Il2CppRGCTXDataType)3, 26756 },
	{ (Il2CppRGCTXDataType)2, 28568 },
	{ (Il2CppRGCTXDataType)3, 26757 },
	{ (Il2CppRGCTXDataType)3, 26758 },
	{ (Il2CppRGCTXDataType)2, 21365 },
	{ (Il2CppRGCTXDataType)2, 28569 },
	{ (Il2CppRGCTXDataType)3, 26759 },
	{ (Il2CppRGCTXDataType)3, 26760 },
	{ (Il2CppRGCTXDataType)2, 21367 },
	{ (Il2CppRGCTXDataType)2, 28570 },
	{ (Il2CppRGCTXDataType)3, 26761 },
	{ (Il2CppRGCTXDataType)3, 26762 },
	{ (Il2CppRGCTXDataType)2, 28571 },
	{ (Il2CppRGCTXDataType)3, 26763 },
	{ (Il2CppRGCTXDataType)3, 26764 },
	{ (Il2CppRGCTXDataType)2, 28572 },
	{ (Il2CppRGCTXDataType)3, 26765 },
	{ (Il2CppRGCTXDataType)3, 26766 },
	{ (Il2CppRGCTXDataType)2, 21382 },
	{ (Il2CppRGCTXDataType)2, 28573 },
	{ (Il2CppRGCTXDataType)3, 26767 },
	{ (Il2CppRGCTXDataType)3, 26768 },
	{ (Il2CppRGCTXDataType)3, 26769 },
	{ (Il2CppRGCTXDataType)3, 25475 },
	{ (Il2CppRGCTXDataType)2, 28574 },
	{ (Il2CppRGCTXDataType)3, 26770 },
	{ (Il2CppRGCTXDataType)3, 26771 },
	{ (Il2CppRGCTXDataType)2, 28575 },
	{ (Il2CppRGCTXDataType)3, 26772 },
	{ (Il2CppRGCTXDataType)3, 26773 },
	{ (Il2CppRGCTXDataType)2, 21398 },
	{ (Il2CppRGCTXDataType)2, 28576 },
	{ (Il2CppRGCTXDataType)3, 26774 },
	{ (Il2CppRGCTXDataType)3, 26775 },
	{ (Il2CppRGCTXDataType)3, 26776 },
	{ (Il2CppRGCTXDataType)3, 26777 },
	{ (Il2CppRGCTXDataType)3, 26778 },
	{ (Il2CppRGCTXDataType)3, 26779 },
	{ (Il2CppRGCTXDataType)3, 25481 },
	{ (Il2CppRGCTXDataType)2, 28577 },
	{ (Il2CppRGCTXDataType)3, 26780 },
	{ (Il2CppRGCTXDataType)3, 26781 },
	{ (Il2CppRGCTXDataType)2, 28578 },
	{ (Il2CppRGCTXDataType)3, 26782 },
	{ (Il2CppRGCTXDataType)3, 26783 },
	{ (Il2CppRGCTXDataType)3, 26784 },
	{ (Il2CppRGCTXDataType)3, 26785 },
	{ (Il2CppRGCTXDataType)3, 26786 },
	{ (Il2CppRGCTXDataType)3, 26787 },
	{ (Il2CppRGCTXDataType)2, 28579 },
	{ (Il2CppRGCTXDataType)2, 28580 },
	{ (Il2CppRGCTXDataType)3, 26788 },
	{ (Il2CppRGCTXDataType)2, 21433 },
	{ (Il2CppRGCTXDataType)2, 21427 },
	{ (Il2CppRGCTXDataType)3, 26789 },
	{ (Il2CppRGCTXDataType)2, 21426 },
	{ (Il2CppRGCTXDataType)2, 28581 },
	{ (Il2CppRGCTXDataType)3, 26790 },
	{ (Il2CppRGCTXDataType)3, 26791 },
	{ (Il2CppRGCTXDataType)3, 26792 },
	{ (Il2CppRGCTXDataType)2, 28582 },
	{ (Il2CppRGCTXDataType)3, 26793 },
	{ (Il2CppRGCTXDataType)2, 21449 },
	{ (Il2CppRGCTXDataType)2, 21441 },
	{ (Il2CppRGCTXDataType)3, 26794 },
	{ (Il2CppRGCTXDataType)3, 26795 },
	{ (Il2CppRGCTXDataType)2, 21440 },
	{ (Il2CppRGCTXDataType)2, 28583 },
	{ (Il2CppRGCTXDataType)3, 26796 },
	{ (Il2CppRGCTXDataType)3, 26797 },
	{ (Il2CppRGCTXDataType)3, 26798 },
	{ (Il2CppRGCTXDataType)2, 28584 },
	{ (Il2CppRGCTXDataType)3, 26799 },
	{ (Il2CppRGCTXDataType)2, 21462 },
	{ (Il2CppRGCTXDataType)2, 21454 },
	{ (Il2CppRGCTXDataType)3, 26800 },
	{ (Il2CppRGCTXDataType)3, 26801 },
	{ (Il2CppRGCTXDataType)2, 21453 },
	{ (Il2CppRGCTXDataType)2, 28585 },
	{ (Il2CppRGCTXDataType)3, 26802 },
	{ (Il2CppRGCTXDataType)3, 26803 },
	{ (Il2CppRGCTXDataType)2, 28586 },
	{ (Il2CppRGCTXDataType)3, 26804 },
	{ (Il2CppRGCTXDataType)2, 21466 },
	{ (Il2CppRGCTXDataType)2, 28587 },
	{ (Il2CppRGCTXDataType)3, 26805 },
	{ (Il2CppRGCTXDataType)3, 26806 },
	{ (Il2CppRGCTXDataType)3, 26807 },
	{ (Il2CppRGCTXDataType)2, 21476 },
	{ (Il2CppRGCTXDataType)3, 26808 },
	{ (Il2CppRGCTXDataType)2, 28588 },
	{ (Il2CppRGCTXDataType)3, 26809 },
	{ (Il2CppRGCTXDataType)3, 26810 },
	{ (Il2CppRGCTXDataType)2, 28589 },
	{ (Il2CppRGCTXDataType)3, 26811 },
	{ (Il2CppRGCTXDataType)2, 21484 },
	{ (Il2CppRGCTXDataType)3, 26812 },
	{ (Il2CppRGCTXDataType)2, 28590 },
	{ (Il2CppRGCTXDataType)3, 26813 },
	{ (Il2CppRGCTXDataType)2, 28590 },
	{ (Il2CppRGCTXDataType)2, 21518 },
	{ (Il2CppRGCTXDataType)3, 26814 },
	{ (Il2CppRGCTXDataType)3, 26815 },
	{ (Il2CppRGCTXDataType)3, 26816 },
	{ (Il2CppRGCTXDataType)3, 26817 },
	{ (Il2CppRGCTXDataType)2, 28591 },
	{ (Il2CppRGCTXDataType)2, 28592 },
	{ (Il2CppRGCTXDataType)2, 28593 },
	{ (Il2CppRGCTXDataType)3, 26818 },
	{ (Il2CppRGCTXDataType)3, 26819 },
	{ (Il2CppRGCTXDataType)2, 21514 },
	{ (Il2CppRGCTXDataType)2, 21517 },
	{ (Il2CppRGCTXDataType)3, 26820 },
	{ (Il2CppRGCTXDataType)3, 26821 },
	{ (Il2CppRGCTXDataType)2, 21521 },
	{ (Il2CppRGCTXDataType)3, 26822 },
	{ (Il2CppRGCTXDataType)2, 28594 },
	{ (Il2CppRGCTXDataType)2, 21511 },
	{ (Il2CppRGCTXDataType)2, 28595 },
	{ (Il2CppRGCTXDataType)3, 26823 },
	{ (Il2CppRGCTXDataType)3, 26824 },
	{ (Il2CppRGCTXDataType)3, 26825 },
	{ (Il2CppRGCTXDataType)2, 28596 },
	{ (Il2CppRGCTXDataType)3, 26826 },
	{ (Il2CppRGCTXDataType)3, 26827 },
	{ (Il2CppRGCTXDataType)3, 26828 },
	{ (Il2CppRGCTXDataType)2, 21536 },
	{ (Il2CppRGCTXDataType)3, 26829 },
	{ (Il2CppRGCTXDataType)2, 28597 },
	{ (Il2CppRGCTXDataType)2, 28598 },
	{ (Il2CppRGCTXDataType)3, 26830 },
	{ (Il2CppRGCTXDataType)3, 26831 },
	{ (Il2CppRGCTXDataType)2, 21558 },
	{ (Il2CppRGCTXDataType)3, 26832 },
	{ (Il2CppRGCTXDataType)2, 21559 },
	{ (Il2CppRGCTXDataType)3, 26833 },
	{ (Il2CppRGCTXDataType)2, 28599 },
	{ (Il2CppRGCTXDataType)3, 26834 },
	{ (Il2CppRGCTXDataType)3, 26835 },
	{ (Il2CppRGCTXDataType)2, 28600 },
	{ (Il2CppRGCTXDataType)3, 26836 },
	{ (Il2CppRGCTXDataType)3, 26837 },
	{ (Il2CppRGCTXDataType)2, 28601 },
	{ (Il2CppRGCTXDataType)3, 26838 },
	{ (Il2CppRGCTXDataType)2, 28602 },
	{ (Il2CppRGCTXDataType)3, 26839 },
	{ (Il2CppRGCTXDataType)3, 26840 },
	{ (Il2CppRGCTXDataType)3, 26841 },
	{ (Il2CppRGCTXDataType)2, 21594 },
	{ (Il2CppRGCTXDataType)3, 26842 },
	{ (Il2CppRGCTXDataType)2, 21602 },
	{ (Il2CppRGCTXDataType)3, 26843 },
	{ (Il2CppRGCTXDataType)2, 28603 },
	{ (Il2CppRGCTXDataType)2, 28604 },
	{ (Il2CppRGCTXDataType)3, 26844 },
	{ (Il2CppRGCTXDataType)3, 26845 },
	{ (Il2CppRGCTXDataType)3, 26846 },
	{ (Il2CppRGCTXDataType)3, 26847 },
	{ (Il2CppRGCTXDataType)3, 26848 },
	{ (Il2CppRGCTXDataType)3, 26849 },
	{ (Il2CppRGCTXDataType)2, 21618 },
	{ (Il2CppRGCTXDataType)2, 28605 },
	{ (Il2CppRGCTXDataType)3, 26850 },
	{ (Il2CppRGCTXDataType)3, 26851 },
	{ (Il2CppRGCTXDataType)2, 21622 },
	{ (Il2CppRGCTXDataType)3, 26852 },
	{ (Il2CppRGCTXDataType)2, 28606 },
	{ (Il2CppRGCTXDataType)2, 21632 },
	{ (Il2CppRGCTXDataType)2, 21630 },
	{ (Il2CppRGCTXDataType)2, 28607 },
	{ (Il2CppRGCTXDataType)3, 26853 },
	{ (Il2CppRGCTXDataType)2, 28608 },
	{ (Il2CppRGCTXDataType)3, 26854 },
	{ (Il2CppRGCTXDataType)3, 26855 },
	{ (Il2CppRGCTXDataType)2, 21639 },
	{ (Il2CppRGCTXDataType)3, 26856 },
	{ (Il2CppRGCTXDataType)2, 21639 },
	{ (Il2CppRGCTXDataType)3, 26857 },
	{ (Il2CppRGCTXDataType)2, 21656 },
	{ (Il2CppRGCTXDataType)3, 26858 },
	{ (Il2CppRGCTXDataType)3, 26859 },
	{ (Il2CppRGCTXDataType)3, 26860 },
	{ (Il2CppRGCTXDataType)2, 28609 },
	{ (Il2CppRGCTXDataType)3, 26861 },
	{ (Il2CppRGCTXDataType)3, 26862 },
	{ (Il2CppRGCTXDataType)3, 26863 },
	{ (Il2CppRGCTXDataType)2, 21636 },
	{ (Il2CppRGCTXDataType)3, 26864 },
	{ (Il2CppRGCTXDataType)3, 26865 },
	{ (Il2CppRGCTXDataType)2, 21641 },
	{ (Il2CppRGCTXDataType)3, 26866 },
	{ (Il2CppRGCTXDataType)1, 28610 },
	{ (Il2CppRGCTXDataType)2, 21640 },
	{ (Il2CppRGCTXDataType)3, 26867 },
	{ (Il2CppRGCTXDataType)1, 21640 },
	{ (Il2CppRGCTXDataType)1, 21636 },
	{ (Il2CppRGCTXDataType)2, 28609 },
	{ (Il2CppRGCTXDataType)2, 21640 },
	{ (Il2CppRGCTXDataType)2, 21638 },
	{ (Il2CppRGCTXDataType)2, 21642 },
	{ (Il2CppRGCTXDataType)3, 26868 },
	{ (Il2CppRGCTXDataType)3, 26869 },
	{ (Il2CppRGCTXDataType)3, 26870 },
	{ (Il2CppRGCTXDataType)3, 26871 },
	{ (Il2CppRGCTXDataType)3, 26872 },
	{ (Il2CppRGCTXDataType)2, 21637 },
	{ (Il2CppRGCTXDataType)3, 26873 },
	{ (Il2CppRGCTXDataType)2, 21652 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	245,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	74,
	s_rgctxIndices,
	352,
	s_rgctxValues,
	NULL,
};
