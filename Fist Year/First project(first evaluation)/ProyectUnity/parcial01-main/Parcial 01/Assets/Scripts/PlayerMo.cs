﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerMo : MonoBehaviour
{
    public static bool mov = true;
    public bool doblesalto;
    public Camera Camara;
    public CamaraMov camare;  
    public GameObject bala;
    public LayerMask Piso;
    public float mover = 8.0f; //declarar la velocidad inicial con la que se va a mover el personaje
    public float Salto;
    public CapsuleCollider col;
    public Rigidbody rb;
    public Text lingotes;
    public Text ganaste;
    public Text cuandogano;
    public Text perdiste;
    public Text vidaa;
    public int cont;
    public int vida;



    void Start()
    {
        vida = 100;
        Cursor.lockState = CursorLockMode.Locked; //bloquear el mouse
        col = GetComponent<CapsuleCollider>();
        rb = GetComponent<Rigidbody>();
        Textos();
        ganaste.text = "";
        perdiste.text = "";



    }
    private void OnCollisionStay()
    {
        doblesalto = true;
    }


    void Update()
    {

        float movimientoHorizontal = Input.GetAxis("Horizontal") * mover; //llamamos en el input el movimiento Horizontal

        float movimientoVertical = Input.GetAxis("Vertical") * mover; //llamamos en el input el movimiento vertical

        movimientoHorizontal *= Time.deltaTime;
        movimientoVertical *= Time.deltaTime;
        
        transform.Translate(movimientoHorizontal, 0, movimientoVertical);


        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray rayo = Camara.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(bala, rayo.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(Camara.transform.forward * 30, ForceMode.Impulse);

            Destroy(pro, 5);


        }
        if (Input.GetKeyDown(KeyCode.Space) && doblesalto)

        {
            rb.AddForce(Vector3.up * Salto, ForceMode.Impulse);
            doblesalto = false;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {

            reinicio();
        }
        caistexd();

    }

    public void caistexd()
    {
        if (transform.position.y < -5)
        {
            SceneManager.LoadScene(1);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.CompareTag("Lingote") == true)
        {
            cont++;
            Textos();
            
            other.gameObject.SetActive(false);
            if (cont == 6)
            {
                fin();
                ganaste.text = "GANASTE!!!! R PARA REINICIAR";
                

            }
        }
        if (other.gameObject.CompareTag("Powerup") == true)
        {
            rb.transform.localScale = new Vector3(2f, 2f, 2f);
            other.gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("Vel") == true)
        {
            mover = 15f;
            other.gameObject.SetActive(false);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            recibirDaño();
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("Bot"))
        {
            recibirDaño();
        }
    }

    private void Textos()
    {
        lingotes.text = "Lingotes = "+ cont.ToString();

    }

    public void recibirDaño()
    {
        vida -= 25;
        textos2();

        if (vida <= 0)
        {
            cuandogano.text = "";
            lingotes.text = "";
            Time.timeScale = 0;
            mov = false;
            perdiste.text = "GAME OVER";

        }
    }
    public void textos2()
    {
        
        vidaa.text = "Vida = " + vida.ToString();
    }
    public void fin()
    {
        cuandogano.text = "";
        lingotes.text = "";
        Time.timeScale = 0;
        mov = false;
    }
    public void reinicio()
    {
        SceneManager.LoadScene(1);
        mov = true;
        Time.timeScale = 1;
    }

}
