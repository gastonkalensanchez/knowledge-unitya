﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bot1 : MonoBehaviour
{
    bool tengoQueBajar = false;
    int rapidez = 10;

    void Update()
    {
        if (transform.position.x >= 11)
        {
            tengoQueBajar = true;
        }
        if (transform.position.x <= -11)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }

    void Subir()
    {
        transform.position += transform.right * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.right * rapidez * Time.deltaTime;
    }

}
