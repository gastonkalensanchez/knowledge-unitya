﻿using UnityEngine;

public class Bot : MonoBehaviour
{
    private int vidabot;

    void Start()
    {
        vidabot = 300;
    }

    public void recibirDaño()
    {
        vidabot -= 100;

        if (vidabot <= 0)
        {
            this.matar();
        }
    }

    private void matar()
    {
        Destroy(gameObject);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            recibirDaño();
        }
    }
}



