﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlJuego : MonoBehaviour
{
    float tiempoRestate;
    public Text textoPerdiste;
    public GameObject player;
    public Rigidbody rb;
    public Text cuandogano;
    public Text lingotes;
    public Text vidaa;

    void Start()
    {
        ComenzarJuego();
    }

    private void Update()
    {
        if (tiempoRestate == 0)
        {
            rb.GetComponent<Rigidbody>().position = new Vector3(42f, 0f, -3f);
            cuandogano.text = "";
            lingotes.text = "";
            vidaa.text = "";
            Time.timeScale = 0;
            PlayerMo.mov = false;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {

            SceneManager.LoadScene(1);
            PlayerMo.mov = true;
            Time.timeScale = 1;
        }
        setearTextos2();
    }
    public void ComenzarJuego()
    {
        StartCoroutine(ComenzarCronometro(60));
    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 60)
    {
        tiempoRestate = valorCronometro;
        while (tiempoRestate >= 0)
        {
            Debug.Log("Faltan " + tiempoRestate + " segundos!");
            yield return new WaitForSeconds(1.0f);
            tiempoRestate--;
        }
    }
    private void setearTextos2()
    {
        if (tiempoRestate <= 0)
        {
            textoPerdiste.text = "Game Over Se te acabo el tiempo";
        }
    }
}