﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bot02 : MonoBehaviour
{
    
    private GameObject jugador;
    public int rapidez = 5;

    void Start()
    {
        
        jugador = GameObject.Find("Player");
    }

    private void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }

}
