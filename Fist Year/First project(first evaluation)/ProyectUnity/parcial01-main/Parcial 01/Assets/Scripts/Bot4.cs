﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bot4 : MonoBehaviour
{

    bool tengoQueBajar = false;
    int rapidez = 5;
    private long vidabot;

    void Start()
    {
        vidabot = 10000000000000;
    }

    public void recibirDaño()
    {
        vidabot -= 100;

        if (vidabot <= 0)
        {
            this.matar();
        }
    }

    private void matar()
    {
        Destroy(gameObject);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            recibirDaño();
        }
    }
    void Update()
    {
        if (transform.position.y >= 3)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= -2)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }

    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }

}
