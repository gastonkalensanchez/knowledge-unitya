﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraMov : MonoBehaviour
{
    Vector2 XY;
    Vector2 Suave;

    public float Sensibilidad = 10.0f;
    public float Suavizado = 2.2f;
    

    GameObject Player;
    
    void Start()
    {
        Player = this.transform.parent.gameObject;

    }


    void Update()
    {

        if (PlayerMo.mov == true)
        {
            var a = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
            a = Vector2.Scale(a, new Vector2(Sensibilidad * Suavizado, Sensibilidad * Suavizado));

            Suave.x = Mathf.Lerp(Suave.x, a.x, 1f / Suavizado);
            Suave.y = Mathf.Lerp(Suave.y, a.y, 1f / Suavizado);

            XY += Suave;
            XY.y = Mathf.Clamp(XY.y, -90f, 90f);
            transform.localRotation = Quaternion.AngleAxis(-XY.y, Vector3.right);
            Player.transform.localRotation = Quaternion.AngleAxis(XY.x, Player.transform.up);
        }

        


        



    }
}
